# PedidosYa Frontend App

Esta aplicación fue escrita en Vue y Typescript.

## Instrucciones para ejecutar

Clonar el proyecto
```javascript
git clone https://gitlab.com/pedidos-ya/frontend.git
```

Instalar dependencias del proyecto
```shell
yarn // or npm install
```

Correr el proyecto

```shell
yarn serve // or npm run serve
```

Si Todo salió bien, deberia ver algo similar a lo siguiente:
```bash
 DONE  Compiled successfully in 470ms

Type checking in progress...

  App running at:
  - Local:   http://localhost:8080/
  - Network: http://192.168.1.45:8080/

No type errors found
```


## Descripción

Aplicación básica para la consulta de restaurantes en base a una ubicacion dada. Posee un login con autenticación, geolocaliza al usuario y muestra restaurantes en base a su ubicación con marcadores en un mapa.


## Justificación de Stack

### Vue
Vue es uno de los frameworks mas livianos y permite maquetar prototipos extremadamente rápido. Elijo Vue sobretodo por su proligidad y su sencillo sistema de componentes que permite abstraer funcionalidades casi trivialmente.

### Vuetify
Vuetify es una libreria de componentes que permite acelerar el prototipado de un sitio y provee funcionalidades como i18n, variables sass predefinidas y temas personalizables, ademas de una extensa lista de componentes que puede ser utilizados a demanda.

### Typescript
Vue es particularmente útil combinado con Typescript, ya que provee una syntaxis mas limpia y clara, ademas de prevenir errores y acelerar la lectura del código.

### SASS
Sass permite definir variables y provee una sintaxis mas limpia y rápida de leer y escribir.