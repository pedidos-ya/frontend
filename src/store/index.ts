import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex)

export default new Vuex.Store({

  state: {
    user: {
      id: '',
      email: '',
      name: ''
    },
    loggedIn: false,
    access_token: null
  },

  mutations: {

    setLoggedIn(s, v) {
      s.loggedIn = v;
    },

    setAccessToken(s, v) {
      s.access_token = v;
    },

    setUserId(s, v) {
      s.user.id = v;
    },

    setUserName(s, v) {
      s.user.name = v;
    },

    setUserEmail(s, v) {
      s.user.email = v;
    }

  },

  actions: {
  },

  modules: {
  },
  plugins: [createPersistedState()]

})
