export interface Restaurant {
    name: string;
    categories: string[];
    rating: number;
    logo: string;
    deliveryTimeMaxMinutes: number;
  }