

import { Vue, Component } from "vue-property-decorator";
import User from '../controllers/Users';

/**
 * @name Page
 * @description this component is the base for every page in the app.
 *              It has the base configuration every component shares.
 */
@Component
export default class Page extends Vue {

    get loggedIn() {
        return this.$store.state.loggedIn;
    }

    set loggedIn(val: boolean) {
        this.$store.commit('setLoggedIn', val);
    }

    get access_token() {
        return this.$store.state.access_token
    }

    set access_token(v: string) {
        this.$store.commit('setAccessToken', v);
    }

    success(title: string, text: string) {
        this.$notify({
            group: 'feedback',
            duration: 5000,
            type: "success",
            title,
            text
        });
    };

    warning(title: string, text: string) {
        this.$notify({
            group: 'feedback',
            duration: 5000,
            type: "warning",
            title,
            text
        });
    };

    error(title: string, text: string) {
        this.$notify({
            group: 'feedback',
            duration: 5000,
            type: "error",
            title,
            text
        });
    };

    info(title: string, text: string) {
        this.$notify({
            group: 'feedback',
            duration: 5000,
            type: "info",
            title,
            text
        });
    };

    async closeSession() {
        try {
            let r = await User.logout();
        } catch (e) {
            console.log(e)
        } finally {
            this.access_token = "";
            this.loggedIn = false;
            sessionStorage.clear();
        }

    }

    /**
     * @description handles http errors
     */
    errorHandler(err: any) {

        console.debug(`[PEYA] ${err}`)
        // Checking if proper http error obj
        if (err && err.status) {
            switch (err.status) {
                case 401:
                    this.closeSession()
                    break;
                case 500:
                    this.warning('Oops, Algo salió mal :(', 'Por favor intente nuevamente mas tarde');
                    break;
                default:
                    this.warning('Oops, Algo salió mal :(', (err.data || {}).message || 'Por favor intente nuevamente más tarde');
            }
        } else {
            this.info('Oops, Algo salió mal :(', 'Por favor intente nuevamente mas tarde');
        }
    };

}