import store from '../store';
import axios, { AxiosResponse } from "axios";

const http = axios.create({
    baseURL: '/api',
    timeout: 8000,
    headers: { 'content-type': 'application/json' }
});

export default class Users {

    static async auth(user: string, password: string): Promise<AxiosResponse> {
        if (!user || !password) {
            throw new TypeError("user and password are not allowed to be empty");
        }

        let r = await http.post('/users/authenticate', { user, password });
        return r.data;
    }

    static async logout() {
        try {
            return await http.post('/users/logout', {
                headers: { 'Authorization': store.state.access_token }
            });

        } catch (e) {
            throw e.response;
        }
    }

    static async getInfo(): Promise<AxiosResponse> {
        try {
            let r = await http.get('/users/account', {
                headers: { 'Authorization': store.state.access_token }
            });
            return r.data;
        } catch (e) {
            throw e.response
        }
    }

}