import store from '../store';
import axios, { AxiosResponse } from "axios";

const http = axios.create({
    baseURL: '/api',
    timeout: 8000,
    headers: { 'content-type': 'application/json' }
});

interface Restaurant {
    name: string;
    categories: string[];
    rating: number;
    logo: string;
    deliveryTimeMaxMinutes: number;
}

export default class Restaurants {

    static async get(options: any): Promise<Restaurant[]> {
        const { point, max } = options;

        if (!point) {
            throw "location data must be provided"
        }

        try {
            let r = await http.get('/restaurants', {
                params: { point, max },
                headers: { 'Authorization': store.state.access_token }
            });
            return r.data;
        } catch (e) {
            throw (e || {}).response;
        }


    }

}