const api = '127.0.0.1:8070';
module.exports = {
  devServer: {
    proxy: {
      "^/api": {
        target: `http://${api}`
      },
    }
  },
  "transpileDependencies": [
    "vuetify"
  ]
}